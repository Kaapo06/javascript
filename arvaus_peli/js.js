const arvattava = Math.floor(Math.random() * 10) + 1;
let arvaukset = 3;

function arvo(arvaus) {
    arvaukset--;

    if (arvaus == arvattava) {
        viesti("Oikein meni!");
    } else if (arvaukset > 0) {
        viesti(`Yritä uudelleen! ${arvaus < arvattava ? 'Luku on suurempi.' : 'Luku on pienempi.'} Yrityksiä jäljellä: ${arvaukset}`);
    } 
    else {
        viesti(`Hävisit! Oikea luku oli ${arvattava}.`);
    }
}

function viesti(viesti) {
    document.getElementById("viesti").innerText = viesti;
}
